## 全局设置菜单
- 完善菜单高亮代码  
- 全局设置菜单高亮  
- 表格渲染  
- 学会 v-for 指令  
- 通过模板语法 {{ }} 设置表格数据  
- 完成搜索区域  
###### 前端样式
```html
<div style="margin: 10px 0">
  <div class="row g-3 align-items-center">
      <div class="col-auto">
          <label class="col-form-label">姓名</label>
      </div>
      <div class="col-auto">
          <input type="text" class="form-control">
      </div>

      <div class="col-auto">
          <label class="col-form-label">手机号</label>
      </div>
      <div class="col-auto">
          <input type="password" class="form-control" aria-describedby="passwordHelpInline">
      </div>
  </div>
</div>
```

了解 ES6的模板字符串语法  

了解 @RequestParam 注解  

实现根据参数动态查询，完成动态SQL编写，实现动态参数查询  