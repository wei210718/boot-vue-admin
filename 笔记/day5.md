# 第五天：统一返回结果和全局异常处理
## Jqury资源文件（前端提示信息插件）
前台替换alert，跟换提示界面
message.css  
message.min.js  
jquery-3.6.0.min.js
只需引入message.min.js和jquery-3.6.0.min.js,但要先引入jquery-3.6.0.min.js。
示例
```html
$.message({message:"请输入账号密码",type:"warning"})
```
## settimeout
```
if (res === "SUCCESS"){
   $.message({message:"登录成功",type:"success"})
    setTimeout(()=>{
    location.href="/"
    },1000)
} else {
    $.message({message:"账号或密码错误",type:"error"})
}
```
## lombok
import lombok.DATA使用@Data注解，简化java代码，省去get set方法
## 封装统一的返回结果
- 封装Result对象（使用泛型）
- 封装几个常用的方法（success error）
- 
## 统一异常处理
- 在业务类里面抛出异常，被全局捕获，返回到前台
- 通过@ResponseBody 转换对象成json
@ControllerAdvice注解，检查全局的异常
## document.write
```html
document.write{'
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-icons.css">
    <script rel="script" src="js/vue.min.js"></script>
    <script rel="script" src="js/bootstrap.bundle.min.js"></script>
    <script rel="script" src="js/jquery-3.6.0.min.js"></script>
    <script rel="script" src="js/message.min.js"></script>
'  
}
```


