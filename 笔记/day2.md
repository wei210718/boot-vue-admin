# 第二天：springboot和mybatis实现后台登录
## Hutool工具类(https://hutool.cn/docs)
#### hutool依赖
```xml
<dependency>
     <groupId>cn.hutool</groupId>
     <artifactId>hutool-all</artifactId>
     <version>5.8.0</version>
 </dependency>
```
#### JsonUtil
JSONUtil是针对JSONObject和JSONArray的静态快捷方法集合。

## debug
##### 前端Debug
先在sources画面打上断点，再点击页面查询按钮，进入Debug模式：
- 1、（三角形）debug执行键–>基本类似于后台debug的调试键（等号）–>暂停脚本执行
- 2、（半弧箭头）step 快捷键：F9–>单步执行 注意：遇到子函数会进去继续单步执行
- 3、（下箭头）step into 快捷键：F11–>单步执行，遇到子函数就进去继续单步执行（上箭头）step over 快捷键：F10–>单步执行，遇到子函数并不进去，将子函数执行完并将其作为一个单步（右箭头）step out 快捷键：Shift + F11–>直接跳出当前的函数，返回父级函数
- 4、（右粗箭头，点击一下多一个斜线，再次点击就取消了）–>禁用断点/启用断点（圆形等号）–>暂停执行
- 5、Watch:在调试画面需要查询返回值或者参数详细信息的时候，可以选中相应的部分单击鼠标右键，再点击Add selected text to watches ,即可在这里看到我们选中部分的详细信息；
#####后端Debug
① 以Debug模式启动服务，左边的一个按钮则是以Run模式启动。在开发中，我一般会直接启动Debug模式，方便随时调试代码。

② 断点：在左边行号栏单击左键，或者快捷键Ctrl+F8 打上/取消断点，断点行的颜色可自己去设置。

③ Debug窗口：访问请求到达第一个断点后，会自动激活Debug窗口。如果没有自动激活，可以去设置里设置，如图1.2。

④ 调试按钮：一共有8个按钮，调试的主要功能就对应着这几个按钮，鼠标悬停在按钮上可以查看对应的快捷键。在菜单栏Run里可以找到同样的对应的功能，如图1.4。

⑤ 服务按钮：可以在这里关闭/启动服务，设置断点等。

⑥ 方法调用栈：这里显示了该线程调试所经过的所有方法，勾选右上角的[Show All Frames]按钮，就不会显示其它类库的方法了，否则这里会有一大堆的方法。

⑦ Variables：在变量区可以查看当前断点之前的当前方法内的变量。

⑧ Watches：查看变量，可以将Variables区中的变量拖到Watches中查看 
## JDBCUtil:连接数据库

代码如下:
```java
public class JDBCUtil {
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/boot-vue-admin?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2b8";
        return DriverManager.getConnection(url,"root","210718WEI");
    }
    public User executeQuery(String username,String password) throws SQLException {
        String sql = "select * from user where username="+ username +"and password="+password;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()){
                String username1 = resultSet.getString("username");
                String password1 = resultSet.getString("password");
                User user = new User();
                user.setUsername(username1);
                user.setPasswodd(password1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            if (resultSet != null){
                resultSet.close();
            }
            if (statement != null){
                statement.close();
            }
            if (connection != null){
                connection.close();
            }
        }
        return null;
    }

}
```
## public static void和public void的区别
- 1.public void 修饰是非静态方法，该类方法属于对象，在对象初始化（new Object()）后才能被调用；void是表示要定义的这个方法没有返回值。
- 2.public static void 修饰是静态方法，属于类，使用【类名.方法名】直接调用。public static是不完整的修饰符，后面要加上void,String,int等类型，表示方法是静态方法。
## JDBC升级懒人ORM-mybatis(https://mybatis.net.cn/)
#####1、什么是mybatis?
MyBatis 是一款优秀的持久层框架，它支持自定义 SQL、存储过程以及高级映射。MyBatis 免除了几乎所有的 JDBC 代码以及设置参数和获取结果集的工作。MyBatis 可以通过简单的 XML 或注解来配置和映射原始类型、接口和 Java POJO（Plain Old Java Objects，普通老式 Java 对象）为数据库中的记录。
#####2、什么是namespace？
命名空间的作用有两个，一个是利用更长的全限定名来将不同的语句隔离开来，同时也实现了你上面见到的接口绑定。就算你觉得暂时用不到接口绑定，你也应该遵循这里的规定，以防哪天你改变了主意。 长远来看，只要将命名空间置于合适的 Java 包命名空间之中，你的代码会变得更加整洁，也有利于你更方便地使用 MyBatis。

命名解析：为了减少输入量，MyBatis 对所有具有名称的配置元素（包括语句，结果映射，缓存等）使用了如下的命名解析规则。

- 全限定名（比如 “com.mypackage.MyMapper.selectAllThings）将被直接用于查找及使用。
- 短名称（比如 “selectAllThings”）如果全局唯一也可以作为一个单独的引用。 如果不唯一，有两个或两个以上的相同名称（比如 “com.foo.selectAllThings” 和 “com.bar.selectAllThings”），那么使用时就会产生“短名称不唯一”的错误，这种情况下就必须使用全限定名。
#####3、学习`<select>`标签

```xml
<dependency>
   <groupId>org.mybatis.spring.boot</groupId>
   <artifactId>mybatis-spring-boot-starter</artifactId>
   <version>2.2.2</version>
</dependency>
```
```properties
mybatis.mapper-locations=classpath:mapper/*.xml
mybatis.configuration.log-impl=org.apache.ibatis.logging.stdout.StdOutImpl
```
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.example.bootvueadmin.mapper.UserMapper">
    <select id="selectUser" resultType="com.example.bootvueadmin.entity.User">
    select * from User where username = #{username} and password = #{password}
  </select>
</mapper>
```
演示错误:

Description:

Field userMapper in com.example.bootvueadmin.controller.UserController required a bean of type 'com.example.bootvueadmin.mapper.UserMapper' that could not be found.

Action:

Consider defining a bean of type 'com.example.bootvueadmin.mapper.UserMapper' in your configuration.

解决办法:

usermapper添加注解@Mapper



