# 第四天：过滤器和登录注册完整逻辑
## HttpServletRequest request
浏览器和服务器之间对话
- request.getSession().getAttribute("user",user)  
getSession()方法相当于得到一个session对象，void setAttribute()和String Attribute分别是对属性赋值和得到属性值的方法。
equest.getSession().setAttribute和request.getSession().getAttribute()就是分别对session对象赋值和得到对象属性的值

## Springboot Filter(https://cloud.tencent.com/developer/article/1525694)
解决用户未登录直接进入主页面
- import javax.servlet.Filter
```java
package com.example.bootvueadmin.common;


import com.example.bootvueadmin.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class AuthFilter implements Filter {
    //白名单，表示应该放行的请求
    private static final String[] WHITE_URLS = {"/login.html","/register.html","/user/login","/user/register"};
    private static final String[] FILE_SUFFIX = {"jpeg","jpg","png","gif","bmp","webp","css","js","woff","woff2","map","ico"};

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String servletPath = request.getServletPath();
//        System.out.println(servletPath);
        if (Arrays.stream(WHITE_URLS).anyMatch(url->url.equals(servletPath)||endWith(servletPath))){ //java8 stream api
            filterChain.doFilter(request,response);
        }else {
            Object user = request.getSession().getAttribute("user");
            if (user!=null){
                filterChain.doFilter(request,response);//放行请求
            }else{
                response.sendRedirect("/login.html");
            }
        }

    }

    private boolean endWith(String path) {
        for(String fileSuffix : FILE_SUFFIX){
            if(path.endsWith(fileSuffix)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void destroy() {

    }
}

```
filter 重定向
HttpServletResponse response = (HttpServletResponse) servletResponse;
response.sendRedirect("/login.html");
## 记录一次错误
登录过程中request.getSession().setAttribute("user",user)错写成 request.getSession().getAttribute("user")导致浏览器未储存用户信息
因此在登录主页后Object user = request.getSession().getAttribute("user");获取用户的信息时，user始终为空值，response.sendRedirect("/login.html");
一直重定向到登录界面。  
前后端不分离的项目每一次重启，session数据存储在服务器端，session都会被清空。
fetch访问后台后，location.href = url 重定向后，浏览器回存在缓存，导致用户退出后user信息仍存在，可采用后台重定向到登录界面response.sendRedirect("/login.html")
## Service包 存放业务操作
首先添加@RestController  
userController通过@Autowired private UserService userService;来使用Service
## Log排查错误
import org.slf4j.Logger;
private static final Logger logger = LoggerFactory.getLogger(UserService.class);
```
public Boolean register(String username, String password) {
        try {
            User user = userMapper.selectUserByUsername(username);
            if (user == null){
                User savedUser = new User(username,password);
                userMapper.save(savedUser);
            }
            return true;
        }catch (Exception e){
            logger.error("注册失败",e);
            return false;
        }
    }
    
```