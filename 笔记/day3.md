# 第三天：Vue和BootStrap改造登录界面
## Vue.js v2.0 (https://v2.cn.vuejs.org/v2/guide/)
##### Vue语法
根div：<div id="app"><div>  
绑值语法：{{message}}  
双向绑值语法：v-model  
事件绑定：v-on  缩写：@    
vue语法示例：  
```vue
<script>
    new Vue({
        el: '#app',
        data() {
            return {
            username: '',
            password: ''
            }
        },
        methods: {
            login(){
                if (!this.username || !this.password){
                    alert("请输入账号和密码")
                    return
                }
                //发送请求给后台
                fetch("user/login",{
                    method:'POST',
                    headers:{
                        'Content-type':'application/json'
                    },
                    body:JSON.stringify({username:this.username,password:this.password})
                }).then(res => res.text()) //先对res进行处理
                    .then(res => {    //res来自后台返回的结果
                        if (res === "SUCCESS")
                            location.href="/"
                        else alert("账号或密码错误!")
                    })
            }
        }
    })
</script>
```
## Bootstrap(https://v5.bootcss.com/docs)
添加文件到static文件夹  
css:bootstrap.min.css
js:bootstrap.bundle.min.js 
引入示例：
```html
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.bundle.min.js"></script>
``` 
图标：bootstrap:icon  
- bootstrap-icon.css
- bootstrap.min.css
- package:font
```html
<i class="bi bi-lock-fill">//添加button
```


