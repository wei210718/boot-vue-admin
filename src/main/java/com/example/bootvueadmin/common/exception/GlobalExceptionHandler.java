package com.example.bootvueadmin.common.exception;

import com.example.bootvueadmin.common.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionHandler {
    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    @ExceptionHandler(CustomerException.class)
    @ResponseBody
    public Result sqlException(CustomerException e){
        logger.error(e.getMessage(),e);
        return Result.error(e.getMessage());
    }


    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result<Void> exception(Exception e) {
        logger.error("系统错误",e);
        return Result.error("系统错误");
    }
}
