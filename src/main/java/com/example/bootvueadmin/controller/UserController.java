package com.example.bootvueadmin.controller;


import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.example.bootvueadmin.Service.UserService;
import com.example.bootvueadmin.common.Result;
import com.example.bootvueadmin.entity.User;
import com.example.bootvueadmin.mapper.UserMapper;
import com.example.bootvueadmin.utils.JDBCUtil;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@RequestMapping("/user")
@RestController
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public Result<User> login(@RequestBody User user ,HttpServletRequest request) throws SQLException {
        //{"username":"admin","password":"admin"}
//        System.out.println(userStr);
//        JSONObject userObj = JSONUtil.parseObj(userStr);//把前台发过来的json数据转换成java对象
//        String username = userObj.getStr("username");
//        String password = userObj.getStr("password");
        //通过mybatis查询user数据
//        User user = userMapper.selectUser(username,password);
        User res = userService.login(user.getUsername(),user.getPassword());
        if(res != null){
            request.getSession().setAttribute("user",res);
        }
        return Result.success(res);
        //通过JDBC查询user数据
//        User user = JDBCUtil.executeQueryUser(username,password)
    }
    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public Result<Void> register(@RequestBody User user) throws SQLException {
        //{"username":"admin","password":"admin"}
//        System.out.println(userStr);
//        JSONObject userObj = JSONUtil.parseObj(userStr);//把前台发过来的json数据转换成java对象
//        String username = userObj.getStr("username");
//        String password = userObj.getStr("password");
        //通过mybatis查询user数据
//        User user = userMapper.selectUser(username,password);
        userService.register(user.getUsername(),user.getPassword());
        return Result.success();
        //通过JDBC查询user数据
//        User user = JDBCUtil.executeQueryUser(username,password)
    }


    @RequestMapping(value = "/logout",method = RequestMethod.GET)
    public void logout(HttpServletRequest request,HttpServletResponse response) throws IOException {
        request.getSession().removeAttribute("user");
//        System.out.println("session=="+request.getSession());
        response.sendRedirect("/login.html"); //推荐后台跳转，防止缓存

    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public Result<List<User>> findAll(String name,
                                      @RequestParam(required = false) String phone){

        return Result.success(userService.findAll(name,phone));
    }
    @RequestMapping(value = "",method = RequestMethod.POST)
    public Result<Void> add(@RequestBody User user) {
        userService.save(user);
        return Result.success();
    }
    @RequestMapping(value = "",method = RequestMethod.PUT)
    public Result<Void> update(@RequestBody User user){
        userService.update(user);
        return Result.success();
    }
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public Result<Void> delete(@PathVariable Integer id){
        userService.deleteById(id);
        return Result.success();
    }
//    @RequestMapping(value = "/password",method = RequestMethod.POST)
//    public Result<Void> password(@RequestBody User user){
//        userService.updatepassword();
//        return Result.success();
//    }
}
