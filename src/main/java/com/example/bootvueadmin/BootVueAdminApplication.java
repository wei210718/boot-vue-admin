package com.example.bootvueadmin;

import com.example.bootvueadmin.common.AuthFilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class BootVueAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootVueAdminApplication.class, args);
    }
    @Bean
    public FilterRegistrationBean<AuthFilter> orderFilter() {
        FilterRegistrationBean <AuthFilter> bean = new FilterRegistrationBean<AuthFilter>(new AuthFilter());
        bean.addUrlPatterns("/*");
        return bean;
    }
}
