package com.example.bootvueadmin.utils;

import com.example.bootvueadmin.entity.User;

import java.sql.*;

public class JDBCUtil {//连接数据库
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/boot-vue-admin?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2b8";
        return DriverManager.getConnection(url,"root","210718WEI");
    }
    public static User executeQueryUser(String username,String password) throws SQLException {
        String sql = "select * from user where username='"+ username +"'and password='"+password+"'";
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()){
                //获取数据库中user信息
                String username1 = resultSet.getString("username");
                String password1 = resultSet.getString("password");
                Integer id1 = resultSet.getInt("id");
                User user = new User();
                user.setId(id1);
                user.setUsername(username1);
                user.setPassword(password1);
                return user;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            if (resultSet != null){
                resultSet.close();
            }
            if (statement != null){
                statement.close();
            }
            if (connection != null){
                connection.close();
            }
        }
        return null;
    }

}
