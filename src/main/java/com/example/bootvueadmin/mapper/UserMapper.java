package com.example.bootvueadmin.mapper;

import com.example.bootvueadmin.common.Result;
import com.example.bootvueadmin.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    User selectUser(@Param("username") String username,@Param("password") String password);

    User selectUserByUsername(String username);

    int save(User savedUser);

    List<User> selectAll(@Param("name")String name,@Param("phone")String phone);

    void update(User user);



    void deleteById(Integer id);
}
