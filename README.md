# 用户信息管理

#### 介绍
前后端不分离的用户信息管理系统  
My first project in gitee

#### 技术栈
- springboot
- vue
- mybatis
- bootstrap

#### 系统界面
![登录界面](%E9%A1%B9%E7%9B%AE%E6%88%AA%E5%9B%BE/login.jpg)
![主界面](%E9%A1%B9%E7%9B%AE%E6%88%AA%E5%9B%BE/index.jpg)
